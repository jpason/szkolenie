#!/bin/bash

MPATH='/mnt/dysk'
mkdir -p "$MPATH"

dd if=/dev/zero of=~/disk.img bs=1024k count=50
mkfs.ext4 -N 100 ~/disk.img
mount -o loop ~/disk.img "$MPATH"

mkdir -p ${MPATH}/log/log{1..5}/{a..z}
find "$MPATH" -type d -exec touch {}/log \;
