#!/usr/bin/env bash

set -o noexec

if [[ -n $1 ]]
then
    echo "Creating file ${1}"
    touch $1
fi 

